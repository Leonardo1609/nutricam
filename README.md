# NutriCÁM
## Considerations for each folder

## food-recognition-api
- Have installed python 3.8+
- Install all libraries listed in the file requirements.txt inside of the folder
- Run **_python app.py_**

## backend
- Have installed python 3.8+
- Install all libraries listed in the file requirements.txt inside of the folder
- Have installed SQL Server 2019 and a database restored with the file .bacpac of this root folder
- Configure the environment variables in db.py and config.py
- Run **_python manage.py runserver_**

## app
- Have installed Nodejs 14+
- Have installed Expo Cli
- Run **_npm install_** for install the node modules dependencies
- Configure the environment.js file with the endpoints of the 2 first executed proyects (maintain the "api" string in the const rest_api_url)
- Run **_expo start_**