from decouple import config
from datetime import timedelta
import os

class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY', '')

class DevelopmentConfig( Config ):
    DEBUG = True
    PROPAGATE_EXECPTIONS = True
    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY", '')
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=15) 

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME", '')
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD", '')
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True

class ProductionConfig( Config ):
    PROPAGATE_EXECPTIONS = True
    JWT_SECRET_KEY = os.environ.get("JWT_SECRET_KEY", '')
    JWT_ACCESS_TOKEN_EXPIRES = timedelta(days=15) 

    MAIL_SERVER = 'smtp.gmail.com'
    MAIL_PORT = 465
    MAIL_USERNAME = os.environ.get("MAIL_USERNAME", '')
    MAIL_PASSWORD = os.environ.get("MAIL_PASSWORD", '')
    MAIL_USE_TLS = False
    MAIL_USE_SSL = True
    
config = {
    'development': DevelopmentConfig,
    'default': DevelopmentConfig,
    'production': ProductionConfig
}
