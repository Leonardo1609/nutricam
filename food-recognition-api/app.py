from flask import Flask, jsonify, request
from werkzeug.utils import secure_filename
from recognition import predict
from flask_cors import CORS
import os
import shortuuid

app = Flask(__name__)
cors = CORS( app )

@app.route('/', methods=['POST'])
def index():
    if request.files.__len__() == 0 or not 'image' in request.files.to_dict().keys():
        return jsonify({ 'msg': 'La imagen es requerida', 'err': 'not_image' }), 400

    food_img = request.files['image']
    file_name = secure_filename(food_img.filename)

    extension = file_name.split('.')[-1]
    if extension not in ['jpg', 'jpeg', 'jfif', 'png']:
        return jsonify({ 'msg': 'Ingrese un formato de imagen válido: jpg, jpeg, jfif o png', 'err': 'invalid_format' }), 400

    path = os.path.join(os.path.dirname(__file__), "temp", f"{shortuuid.uuid()}-{file_name}")

    try:
        food_img.save(path)
        foods = predict(path)
        os.remove(path) 
        return jsonify({ 'recognized_foods': foods }), 200 
    except:
        os.remove(path) 
        return jsonify({ 'msg': 'Ha ocurrido un error' }), 500

if __name__ == '__main__':
    app.run()